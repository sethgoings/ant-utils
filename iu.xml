<?xml version="1.0" encoding="UTF-8"?>
<project name="iu" xmlns:ivy="antlib:org.apache.ivy.ant">
  <dirname property="iu.basedir" file="${ant.file.iu}"/>

  <!-- IVY HELPERS -->

  <!-- Load their file first, to override our choices -->
  <property file="${basedir}/ivy.properties"/>
  <property file="${iu.basedir}/ivy.properties"/>
  
  <property file="${basedir}/build.properties"/>
  <property file="${iu.basedir}/build.properties"/>
  
  <condition property="ivy.deliver.branch" value="${branch}">
    <isset property="branch"/>
  </condition>
  
  <target name="buildlist" depends="init-ivy">
    <ivy:buildlist reference="build-path"
                   root="${ivy.module}" 
                   onMissingDescriptor="skip"
                   excludeRoot="true">
      <fileset dir="${basedir}/../" includes="*/build.xml"/>
    </ivy:buildlist>
    <echo message="Build list is: ${toString:build-path}"/>
  </target>

  <target name="check-ivy">
    <available property="ivy_downloaded" file="${ivy.jar.file}"/>
  </target>

  <target name="download-ivy" depends="check-ivy" unless="ivy_downloaded">
    <!--
        Download ivy if it has not already been downloaded.
    -->
    <mkdir dir="${ivy.jar.dir}" />
    <get src="http://repo2.maven.org/maven2/org/apache/ivy/ivy/${ivy.version}/ivy-${ivy.version}.jar"
         dest="${ivy.jar.file}" usetimestamp="true"/>
  </target>

  <target name="init-ivy" depends="iu.download-ivy" unless="ivy.initialized">
    <!--
        Initializes ivy.
    -->
    <private-init-ivy/>
  </target>
  
  <macrodef name="private-init-ivy">
    <!--
        Does the work for init-ivy as a macrodef.
    -->
    <sequential>
      <fail>
        <condition>
          <not>
            <available file="${ivy.jar.file}"/>
          </not>
        </condition>
      </fail>
      <path id="ivy.lib.path">
        <fileset dir="${ivy.jar.dir}" includes="*.jar" />
      </path>
      <taskdef resource="org/apache/ivy/ant/antlib.xml"
               uri="antlib:org.apache.ivy.ant"
               classpathref="ivy.lib.path" />
      <property name="ivy.initialized" value="true"/>
    </sequential>
  </macrodef>
  
  <macrodef name="private-init-antutil">
    <sequential>
      <fail>
        <condition>
          <not>
            <available file="${au.lib.dir}/ant-contrib-${au.plugins.antcontrib.rev}.jar"/>
          </not>
        </condition>
      </fail>
      <!--
      Normally we would prefer to use the classpath directly, but in this case
      we don't get access to the methods that allow for that restructuring
      until after this is loaded.
      -->
      <taskdef resource="net/sf/antcontrib/antcontrib.properties">
      <classpath>
        <pathelement location="${au.lib.dir}/ant-contrib-${au.plugins.antcontrib.rev}.jar"/>
      </classpath>
    </taskdef>
    </sequential>
  </macrodef>

  <target name="resolve" depends="init-ivy">
    <!-- ivy:resolve downloads artifacts to cache -->
    <ivy:resolve file="${ivy.file}"/>
    <!-- ivy:retrieve copies artifacts in project directory -->
    <ivy:retrieve pattern="${ivy.retrieve.pattern}" />
    <ivy:cachepath pathid="iu.resolve.classpath" />
  </target>
  
  <target name="-load-antutil" depends="init-ivy" unless="au.classpath.antutil">
    <resolve-antutil/>
  </target>
  
  <macrodef name="resolve-antutil">
    <!--
        Download ivy and initialize things, then load
        in antutil.
    -->
    <sequential>
      <private-init-ivy/>
      <ivy:resolve file="${iu.basedir}/plugins.xml"/>
      <ivy:retrieve pattern="${iu.basedir}/lib/[artifact]-([branch]-)[revision].[ext]"
          conf="antutil"/>
      <ivy:cachepath pathid="au.classpath.antutil"/>
      <private-init-antutil/>
    </sequential>
  </macrodef>

  <macrodef name="resolve-helper">
    <attribute name="confs"/>
    <sequential>
      <ivy:retrieve pattern="${ivy.retrieve.pattern}" conf="@{confs}" />
    </sequential>
  </macrodef>

  <target name="resolve-sync" depends="init-ivy">
    <!-- ivy:resolve downloads artifacts to cache -->
    <ivy:resolve file="${ivy.file}"/>
    <!-- ivy:retrieve copies artifacts in project directory -->
    <ivy:retrieve pattern="${ivy.retrieve.pattern}" sync="true" />
  </target>

  <target name="local-version" depends="init-ivy">
    <tstamp>
      <format property="now" pattern="yyyyMMddHHmmss"/>
    </tstamp>
<!--    <property name="ivy.revision" value="${version.target}"/>-->
    <property name="ivy.new.revision" value="${version.target}-local-${now}"/>
    <echo>New version: ${ivy.new.revision}</echo>
    <echo>Old version: ${ivy.revision}</echo>
  </target>

  <!-- This command should only be used for deployments to artifactory, as it can take a while ... -->
  <target name="new-version" depends="init-ivy" unless="ivy.new.revision">
    <ivy:info file="${ivy.file}" />
    <ivy:settings id="ecovate-publish-chain" file="${ivy.settings.dir}/ivysettings-ecovate-publish-chain.xml"/>
    <echo>Building new version number... (contacting repositories)</echo>

    <property name="module.version.prefix" value="${version.target}" />
    <ivy:buildnumber
          organisation="${ivy.organisation}"
          module="${ivy.module}"
          revision="${module.version.prefix}"
          defaultBuildNumber="0"
          revSep="."
          settingsRef="ecovate-publish-chain"/>
    <echo>New version: ${ivy.new.revision}</echo>
  </target>
  
  <target name="save-version" depends="new-version">
    <property name="ivy.new.revision" value="${ivy.new.revision}"/>
    <echo file="version.properties" message="version=${ivy.new.revision}" append="false"/>
  </target>

  <target name="deliver" depends="resolve">
    <ivy:deliver deliverpattern="${dir.build}/ivy.xml"
                 pubrevision="${ivy.new.revision}"/>
  </target>

  <!-- CLEANING HELPERS -->
  <target name="clean-cache">
    <delete dir="${ivy.cache.dir}"/>
  </target>

  <target name="clean-local">
    <delete dir="${ivy.local.dir}"/>
  </target>

  <target name="clean-shared">
    <delete dir="${ivy.shared.dir}"/>
  </target>

  <target name="clean-lib">
    <delete dir="${dir.lib}" quiet="true"/>
  </target>

  <target name="reset-locks">
    <delete>
      <fileset dir="${ivy.cache.dir}">
        <include name="**/*.lck"/>
      </fileset>
    </delete>
  </target>

  <target name="clean-all" depends="clean, clean-lib, clean-local, clean-cache"/>

  <!--   PUBLISH HELPERS -->
  <target name="publish-local" depends="local-version, resolve">
    <ivy:publish resolver="local"
                  haltonmissing="false"
                  warnonmissing="true"
                  pubrevision="${ivy.new.revision}"
                  forcedeliver="true"
                  status="integration">

      <artifacts pattern="${dir.distrib.module}/[artifact].[ext]"/>
    </ivy:publish>
  </target>

  <!-- PUBLISH HELPERS -->
  <!-- You can remove "resolve" as a dependency to this target if you're using ant 1.8.2? (build server still needs to be updated -->
  <target name="publish-release" depends="new-version, resolve">
    <ivy:publish resolver="ecovate-libs-releases"
                  haltonmissing="false"
                  warnonmissing="true"
                  pubrevision="${ivy.new.revision}"
                  status="release">

      <artifacts pattern="${dir.distrib.module}/[artifact].[ext]"/>
    </ivy:publish>
  </target>

  <target name="publish-libs-release" depends="publish-release"/>

  <target name="publish-ext-release" depends="new-version, resolve">
    <ivy:publish resolver="ecovate-ext-releases"
                  haltonmissing="false"
                  warnonmissing="true"
                  pubrevision="${ivy.new.revision}"
                  status="release">

      <artifacts pattern="${dir.distrib.module}/[artifact].[ext]"/>
    </ivy:publish>
  </target>

  <target name="publish-libs-release-with" depends="resolve">
    <ivy:publish resolver="ecovate-libs-releases"
                  haltonmissing="false"
                  warnonmissing="true"
                  pubrevision="${ivy.new.revision}"
                  overwrite="true"
                  forcedeliver="true"
                  status="release">

      <artifacts pattern="${dir.distrib.module}/[artifact].[ext]"/>
    </ivy:publish>
  </target>

    <target name="publish-libs-snap-with" depends="resolve">
    <ivy:publish resolver="ecovate-libs-snapshots"
                  haltonmissing="false"
                  warnonmissing="true"
                  pubrevision="${ivy.new.revision}-SNAPSHOT"
                  overwrite="true"
                  forcedeliver="true"
                  status="integration">

      <artifacts pattern="${dir.distrib.module}/[artifact].[ext]"/>
    </ivy:publish>
  </target>
  
  <target name="publish-ext-release-with" depends="resolve">
    <ivy:publish resolver="ecovate-ext-releases"
                  haltonmissing="false"
                  warnonmissing="true"
                  pubrevision="${ivy.new.revision}"
                  overwrite="true"
                  forcedeliver="true"
                  status="release">

      <artifacts pattern="${dir.distrib.module}/[artifact].[ext]"/>
    </ivy:publish>
  </target>

  <target name="publish-snap" depends="new-version, resolve">
    <ivy:publish resolver="ecovate-libs-snapshots"
                  overwrite="true"
                  srcivypattern="${dir.distrib}/ivy.xml"
                  haltonmissing="false"
                  warnonmissing="true"
                  pubrevision="${ivy.new.revision}-SNAPSHOT"
                  forcedeliver="true"
                  status="integration">

      <artifacts pattern="${dir.distrib.module}/[artifact].[ext]"/>
    </ivy:publish>
  </target>

  <target name="publish-libs-snap" depends="publish-snap"/>

  <target name="report" depends="resolve">
    <ivy:report todir="${dir.resolve.report}"/>
  </target>

  <target name="status">
    <exec executable="git" outputproperty="commit">
      <arg value="show"/>
      <arg value="--summary"/>
      <arg value="--oneline"/>
    </exec>

    <exec executable="git" resultproperty="dirty">
      <arg value="diff-index"/>
      <arg value="--quiet"/>
      <arg value="HEAD"/>
    </exec>

<!--    <property name="ivy.branch" value="${branch}"/>-->
<!--    <exec executable="git" outputproperty="ivy.branch">
      <arg value="name-rev"/>
      <arg value="\-\-name-only"/>
      <arg value="HEAD"/>
    </exec>-->
  </target>

  <!-- NEEDS WORK -->
  <target name="info" depends="init-ivy, status">
    <!-- load generated version properties file -->
    <property file="${dir.distrib.module}/${module}.properties" />
    <ivy:info/>
    <echo>
      Organization: ${ivy.organisation}
      Module: ${ivy.module}
      Branch: ${ivy.branch}
      Old Version: ${ivy.revision}
      New Version: ${ivy.new.revision}
      Old Build Number: ${ivy.build.number}
      New Build Number: ${ivy.new.build.number}
      Status: ${ivy.status}
      Commit: ${commit}
      Dirty?: ${dirty}
    </echo>
  </target>

  <!-- Aliases -->
</project>
